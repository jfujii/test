//
//  AppDelegate.h
//  TEST
//
//  Created by ikobo on 2013/03/25.
//  Copyright (c) 2013年 ikobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
