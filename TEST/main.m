//
//  main.m
//  TEST
//
//  Created by ikobo on 2013/03/25.
//  Copyright (c) 2013年 ikobo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
